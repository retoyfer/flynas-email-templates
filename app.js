const juice = require('juice');
const fs = require('fs');
const path = require('path');
const prettier = require('prettier');

fs.readdir('emails', (err, files) => {
    for(let i = 0; i < files.length; i++) {
        // Skip if directory
        // Skip if not html file
        let stat = fs.lstatSync('emails' + path.sep + files[i]);
        if(stat.isDirectory() || !files[i].includes('.html')) continue;
        console.log(files[i]);

        fs.readFile('emails' + path.sep + files[i], (err, html) => {
            juice.juiceResources(html.toString(), {
                webResources: {
                    relativeTo: 'emails' + path.sep,
                    images: false
                },
                xmlMode: true
            }, (errorJuice, html) => {
                html = prettier.format(html.replace(/class=\"(.*?)\"/s, ''), {
                    "arrowParens": "avoid",
                    "bracketSpacing": true,
                    "htmlWhitespaceSensitivity": "css",
                    "insertPragma": false,
                    "printWidth": 10000,
                    "proseWrap": "preserve",
                    "quoteProps": "as-needed",
                    "requirePragma": false,
                    "semi": true,
                    "singleQuote": false,
                    "tabWidth": 2,
                    "trailingComma": "none",
                    "useTabs": true,
                    "parser": "html"
                });

                fs.writeFile('emails' + path.sep + files[i], html, () => {

                });
            });
        });
    }
});


